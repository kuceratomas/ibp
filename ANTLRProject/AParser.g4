parser grammar AParser;

@header { package ahll.editor; }

options {   tokenVocab = ALexer; }

start :
	EOF | program EOF;

program :
	(storageStatement | plan | service)* main (storageStatement | plan | service)* ;

storageStatement :
	(constant | variable | variableDef) SC ;
	
constant :
	CONSTANT id ASSIGN valueLiteral ;
	
variable :
	VAR (variableDef | id) ( CIARKA (variableDef | id))* ;
	
variableDef :
	id ASSIGN valueLiteral ;
	
/* Callable ----------------------------------------------------------------- */

service :
	SERVICE id LZ idSequence? PZ THROWS? serviceBlock;
	
serviceBlock :
	LZZ serviceStatement PZZ ;
	
serviceStatement:
	returnStatement | callStatement ;
	
returnStatement :
	RETURN callStatement ;
	
callStatement: 
	CALL integralList SC ;
	
plan :
	PLAN id LZ idSequence? PZ block ;
	
main :
	MAIN LZ PZ block ;
	
/* Statements --------------------------------------------------------------- */

block :
	LZZ statement* PZZ ;
	
statement :
	block #blockStatement
	| SEND LZ target=expression CIARKA content=expression PZ SC #sendStatement
	| FI SC #inputEnableStatement
	| OI SC #inputDisableStatement
	| (ifStatement | whileStatement | foreachStatement) #controlStatement
	| expression SC #expressionStatement
	| storageStatement #localStorageStatement
	| variableExpr SC #localVariableStatement
	;
	
variableExpr :
	VAR variableDefExpr (CIARKA variableDefExpr)* ;
	
variableDefExpr :
	id ASSIGN expression ;
	
ifStatement :
	IF LZ expression PZ then=block (ELSE otherwise=block)? ;
	
whileStatement :
	WHILE LZ expression PZ block ;

foreachStatement:
	FOREACH LZ VAR item=id IN (collection=id | valueList) PZ block ;
	
/* Expressions -------------------------------------------------------------- */

expressionList :
	expression ( CIARKA expression )* ;
	
expression :
	integralLiteral #literal
	| LZ expression PZ #bracket
	| id LZ expressionList? PZ #invoke
	| RECEIVE LZ expression? PZ #receive
	| id INC #postIncrement
	| id DEC #postDecrement
	| INC id #preIncrement
	| DEC id #preDecrement
	| ADD expression #plus
	| SUB expression #minus
	| NEG expression #negate
	| expression MUL expression #multiply
	| expression DIV expression #divide
	| expression MOD expression #modulo
	| expression ADD expression #add
	| expression SUB expression #subtract
	| expression LS expression #less
	| expression LSE expression #lessOrEqual
	| expression GT expression #greater
	| expression GTE expression #greaterOrEqual
	| expression EQ expression #equal
	| expression NEQ expression #notEqual
	| expression AND expression #and
	| expression OR expression #or
	| id ASSIGN expression #assign
	;
	
/* Literals ----------------------------------------------------------------- */

integralLiteral : // a, "a", 1, [], [1, []], ...
	id #idIntegralLiteral
	| value #valueIntegralLiteral
	| integralList #listIntegralLiteral
	;
	
integralLiterals :
	(integralLiteral CIARKA)* integralLiteral ;
	
integralList :
	LHZ integralLiterals? PHZ ;
	
idSequence :
	id (CIARKA id )* ;

valueLiteral : // "a", 1, [], [1, []], ...
	value
	| valueList
	;
	
valueLiterals:
	(valueLiteral CIARKA)* valueLiteral ;
	
valueList :
	LHZ valueLiterals? PHZ ;
	
id :
	Identifier ;
	
value :
	IntegerLiteral #integerLiteral
	| StringLiteral #stringLiteral
	;
