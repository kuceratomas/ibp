*******************************************************************************

*******************************************************************************

  I. OBSAH PAMATOVEHO MEDIA

  src/                    zdrojove texty a ANTLR lexikalne pravidla

  lib/                    pouzite frameworky

  Resources/images        obrazky pouzite v GUI
  Resources/lib/Compiler  AHLL prekladac
  Resources/lib/T-Mass    T-Mass simulator

  jar/ahlleditor.jar      spustitelny jar subor

  examples/               ukazkove AHLL kody (prevzate z prace Ing. Kalmara)

  LICENSE.txt             licencie pouzitych frameworkov

  README.txt              subor s informaciami o pouziti


===============================================================================

  II. POZIADAVKY NA PREKLAD A SPUSTENIE

  Spustenie - JRE doporucene 1.8
  Preklad - Apache Ant

===============================================================================

  III. PREKLAD A SPUSTENIE

---

  Spustenie prilozeneho JAR suboru:

  java -jar ahlleditor.jar [FILES]

  volitelny parameter FILES je medzerami oddeleny zoznam suborov zadanych
  absolutnymi alebo relativnymi cestami
  
---

  Preklad prilozenych zdrojovych kodov:

  ant compile

---

  Vytvorenie noveho JAR suboru

  ant jar

---

  Spustenie novo vytvoreneho JAR suboru

  ant run

---

  Zmazanie prelozenych suborov a JAR suboru

  ant clean


