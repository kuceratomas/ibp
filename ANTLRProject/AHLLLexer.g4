lexer grammar AHLLLexer;

COMMENT_EOL					: '//' ~[\r\n]* ;

SC_COMMA_DOT					: ',' | ';' | '.';

STR							: '"' STR_frag '"' ;
fragment STR_frag 			: ('a'..'z'|'A'..'Z'|'0'..'9')* ;

NEWLINE            			: '\r'? '\n' -> channel(1) ;

OPERATOR					: '||' |
							'&&' |
							'!=' |
							'<'  |
							'<=' |
							'>'  |
							'>=' |
							'+'  |
							'-'  |
							'*'  |
							'/'  |
							'%'  |
							'!'  |
							'++' |
							'--' ;

RESERVED_WORD				: 'if' |
							'else' |
							'while' |
							'for' |
							'foreach' |
							'const';

RESERVED_WORD_2				: 'return';

PLAN						: 'plan';

SERVICE						: 'service' ;

LITERAL_NUMBER_DECIMAL_INT	: '0'..'9'+ ;

MESSAGE_ENABLE: 			'FI' |
							'OI';

IN_DO						: 'in' |
							'do' ;

FUNCTION					: 'main' |
							'car' |
							'cdr' |
							'call';
						

NULL						: 'null' | 'NULL' ;	

ASSIGN						: '=' ;		

EQUALS						: '==' ;	

VARIABLE					: 'var' ;

SEND						: 'send' ;

RECEIVE						: 'receive' ;

IDENTIFIER					: (('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')*) ;	

WHITESPACE					: [\t\f ]+ -> channel(1) ;

SEPARATOR					: 	'{' |
							'}' |
							'[' |
							']' |
							')' |
							'(' ;

UNMATCHED          			: . -> channel(1);
