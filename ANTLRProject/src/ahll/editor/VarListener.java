package ahll.editor;

import java.util.Stack;

public class VarListener extends AParserBaseListener {
	
	private Stack<Scope> scopes;

    public VarListener() {
        scopes = new Stack<Scope>();
        scopes.push(new Scope(null));
    }
    
    @Override
    public void exitVariable(AParser.VariableContext ctx) {
    	System.out.println("safdsf");
    }


    @Override
    public void enterBlock(AParser.BlockContext ctx) {
    	System.out.println("safdsf");
    }
    
    @Override
    public void enterProgram(AParser.ProgramContext ctx) {
    	System.out.println("safdsf");
    }
    
    @Override
    public void exitMain(AParser.MainContext ctx) {
    	System.out.println("safdsf");
    }


    private void checkVarName(String varName) {
        Scope scope = scopes.peek();
        System.out.println(varName);
        if(scope.inScope(varName)) {
            System.out.println("OK   : " + varName);
        }
        else {
            System.out.println("Oops : " + varName);
        }
    }

}
