package ahll.editor.actions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import ahll.editor.gui.Frame;
import ahll.editor.gui.TabComponent;
import ahll.editor.gui.TabComponentTab;


public class OpenFileAction implements ActionListener  {
	
	private Frame frame;
	
	public OpenFileAction(Frame frame) {
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Thread openFileThread = new Thread() {
			  public void run() {
				
				  // implicitna cesta vo filechooseri je PWD
				  String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
				  JFileChooser fc = new JFileChooser(currentPath);
				  fc.setDialogTitle("Choose AHLL file to open");
				  FileNameExtensionFilter filter = new FileNameExtensionFilter("Agent High Level Language Files", "ahll");
				  fc.setFileFilter(filter);
				  int returnVal = fc.showOpenDialog(frame);
		    	
		    	 if (returnVal == JFileChooser.APPROVE_OPTION) {
		    		 //bol vybrany subor -> vytvori sa dokument
		    		 
		    		 File file = fc.getSelectedFile();
		            
		    		 TabComponent panel = new TabComponent( frame.getGUIToolBar().getLabel());
		    		 panel.getTextArea().setFile(file);
		    		 frame.getTabbedPane().add(file.getName(), panel);
		
		    		 JPanel pnlTab = new TabComponentTab(file.getName(), frame);
		
		    		 frame.getTabbedPane().setSelectedIndex( frame.getTabbedPane().getTabCount()-1);
		    		 frame.getTabbedPane().setTabComponentAt( frame.getTabbedPane().getTabCount()-1, pnlTab);
		
		    		 try {
		    			 // citanie obsahu suboru do dokumentu
		    			 Scanner scanner = new Scanner(file, "UTF-8" );
		    			 if (scanner.useDelimiter("\\Z").hasNext()) {
		    				 panel.getTextArea().setText(scanner.useDelimiter("\\Z").next());
		    			 }
		    			 panel.getTextArea().requestFocus();
		    			 scanner.close();
		    		 } catch (FileNotFoundException e1) {e1.printStackTrace();}
		    	 }
			 }
		};
		
		openFileThread.start();
    }
}
