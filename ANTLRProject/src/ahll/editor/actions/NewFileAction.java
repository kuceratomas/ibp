package ahll.editor.actions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import ahll.editor.gui.Frame;
import ahll.editor.gui.TabComponent;
import ahll.editor.gui.TabComponentTab;


public class NewFileAction implements ActionListener {
	
	Frame frame;
	
	public NewFileAction(Frame frame) {
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		
		TabComponent panel = new TabComponent(frame.getGUIToolBar().getLabel());
        frame.getTabbedPane().add("New File", panel);

        JPanel pnlTab = new TabComponentTab("New file", frame);

        frame.getTabbedPane().setSelectedIndex(frame.getTabbedPane().getTabCount()-1);
        frame.getTabbedPane().setTabComponentAt(frame.getTabbedPane().getTabCount()-1, pnlTab);
        
        panel.getTextArea().requestFocusInWindow();

	}

}
