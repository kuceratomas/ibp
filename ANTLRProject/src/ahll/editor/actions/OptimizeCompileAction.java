package ahll.editor.actions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import javax.swing.JProgressBar;

import ahll.editor.CompilationLog;
import ahll.editor.gui.Frame;
import ahll.editor.gui.TabComponent;


public class OptimizeCompileAction implements ActionListener{

	Frame frame;
	
	public OptimizeCompileAction(Frame frame) {
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (frame.getTabbedPane().getSelectedIndex() == -1) {
			return;
		}
		
		File file = frame.getActualTabComp().getTextArea().getFile();
		JProgressBar progressBar = frame.getGUIToolBar().getProgressBar();

		if (file == null) {
			return;
		}
		
		// ak ma priponu .ahll nahradi ju .alll, inak sa len pripoji .alll k nazvu
		String fileName = file.getName();
		String newFileName = "new.alll";
		if (0 == fileName.length() - fileName.replace(".ahll", "").length()) {
			newFileName = fileName + ".alll";
		}
		else {
			newFileName= fileName.replace(".ahll", ".alll");
		}
		
		// nastavenie cesty k suboru na PWD
		File newFile = new File(Paths.get(".").toAbsolutePath().normalize().toString() + "\\" + newFileName);
		
		// zoznam moznych optimalizacii
		List<String> parameters = Arrays.asList("-ConstantFolding", "-CopyPropagation", "-DeadCodeElimination");
		
		Thread compileThread = new Thread() {
			// spustanie s optimalizaciami v novom vlakne
			public void run() {
		
				progressBar.setString("Compilation in progress...");
				progressBar.setVisible(true);
				
				long minCodeLenght = Long.MAX_VALUE;
				int minParamIndex = 0;
				
				
				String path = Frame.class.getProtectionDomain().getCodeSource().getLocation().getPath();
				String decodedPath = "";
				try {
					decodedPath = URLDecoder.decode(path, "UTF-8");
				} catch (UnsupportedEncodingException e1) {e1.printStackTrace();}
				
				File file2 = new File(decodedPath);
				decodedPath = file2.getParent();
				file2 = new File(decodedPath);
				
				ProcessBuilder pb = new ProcessBuilder();
				pb.directory(file2);
		
				for (int i = 0; i < 9; i++) {
					// aplikovanie vsetkych kombinacii optimalizacii
					long actualCodeLenght;
					
					if (i < 3) {
						// 2 optimalizacie
						pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString(), "-ConditionalConstantPropagation", parameters.get(i));
						actualCodeLenght = compile(pb, newFile, false, "");
						if (i == 0) {
							minCodeLenght = actualCodeLenght;
						}
						else {
							if (minCodeLenght > actualCodeLenght) {
								minCodeLenght = actualCodeLenght;
								minParamIndex = i;
							}
						}
					}
					else if (i < 6){
						// 3 parametre
						pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString(), "-ConditionalConstantPropagation", parameters.get(i%3), parameters.get((i+1)%3));
						actualCodeLenght = compile(pb, newFile, false, "");
						if (minCodeLenght > actualCodeLenght) {
							minCodeLenght = actualCodeLenght;
							minParamIndex = i;
						}
					}
					else if (i == 6){
						// vsetky parametre
						pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString(), "-ConditionalConstantPropagation", "-ConstantFolding", "-CopyPropagation", "-DeadCodeElimination");
						actualCodeLenght = compile(pb, newFile, false, "");
						if (minCodeLenght > actualCodeLenght) {
							minCodeLenght = actualCodeLenght;
							minParamIndex = i;
						}
					}
					else if (i == 7){
						// iba parameter ConditionalConstantPropagation
						pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString(), "-ConditionalConstantPropagation");
						actualCodeLenght = compile(pb, newFile, false, "");
						if (minCodeLenght >= actualCodeLenght) {
							minCodeLenght = actualCodeLenght;
							minParamIndex = i;
						}
					}
					else {
						// bez optimalizacii
						pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString());
						actualCodeLenght = compile(pb, newFile, false, "");
						if (minCodeLenght >= actualCodeLenght) {
							minCodeLenght = actualCodeLenght;
							minParamIndex = i;
						}
					}
					progressBar.setValue(progressBar.getValue()+1);
					
					// syntakticka chyba
					if (actualCodeLenght == -1) {
						progressBar.setValue(progressBar.getMaximum());
						break;
					}
				}
				
				// este jeden preklad s vybranymi optimalizaciami a vytvorenie suboru s cielovym kodom
				
				if (minParamIndex < 3) {
					pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString(), "-ConditionalConstantPropagation", parameters.get(minParamIndex));
					compile(pb, newFile, true, "-ConditionalConstantPropagation\n" + parameters.get(minParamIndex) + "\n"); 
				}
				else if (minParamIndex < 6){ // 1 parameter
					pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString(), "-ConditionalConstantPropagation", parameters.get(minParamIndex%3), parameters.get((minParamIndex+1)%3));
					compile(pb, newFile, true, "-ConditionalConstantPropagation\n" + parameters.get(minParamIndex%3) + "\n"+ parameters.get((minParamIndex+1)%3) + "\n");
				}
				else if (minParamIndex == 6){ // 1 parameter
					pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString(), "-ConditionalConstantPropagation", "-ConstantFolding", "-CopyPropagation", "-DeadCodeElimination");
					compile(pb, newFile, true, "-ConditionalConstantPropagation\n-ConstantFolding\n-CopyPropagation\n-DeadCodeElimination\n");
				}
				else if (minParamIndex == 7){ // 1 parameter
					pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString(), "-ConditionalConstantPropagation");
					compile(pb, newFile, true, "-ConditionalConstantPropagation\n");
				}
				else {
					pb.command("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString());
					compile(pb, newFile, true, "None\n");
				}
				
				progressBar.setValue(0);
				progressBar.setVisible(false);
			}
		};
		
		compileThread.start();
		
					
}
	
	// kompilacia s porovnavanim optimalizacii + koncovy preklad s vytvorenim suboru (flag createFile)
	private long compile(ProcessBuilder pb, File file, boolean createFile, String optimalizations) {

		try {
			Process p = pb.start();
			
			CompilationLog lsr = new CompilationLog(p.getInputStream(), p.getErrorStream(),
										((TabComponent)frame.getTabbedPane().getComponentAt(frame.getTabbedPane().getSelectedIndex())).getJTextArea(), file, optimalizations);
			
			if (createFile) {
				lsr.intoFile();
			}
			else {
				return lsr.read();
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		return -1;
		
	}
}
