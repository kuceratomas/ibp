package ahll.editor.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.SearchContext;
import org.fife.ui.rtextarea.SearchEngine;
import org.fife.ui.rtextarea.SearchResult;

import ahll.editor.gui.FindReplaceFrame;
import ahll.editor.gui.Frame;

public class FindAction implements ActionListener{
	
	private Frame frame;
	
	public FindAction(Frame frame) {
		this.frame = frame;

	}

	 public void actionPerformed(ActionEvent e) {
		
		if (frame.getTabbedPane().getTabCount() < 1) {
			return;
		}
		
		RSyntaxTextArea rsta = frame.getActualTabComp().getTextArea();
	
		String command = e.getActionCommand();
		boolean forward = !"FindPrev".equals(command); // hladanie vpred/vzad, implicitne vpred
		
		SearchContext context = new SearchContext();
		String text = FindReplaceFrame.getInstance().getSearchField().getText();
		if (text.length() == 0) {
		 return;
		}
		
		context.setMarkAll(false);
		context.setSearchFor(text);
		context.setMatchCase(FindReplaceFrame.getInstance().getMatchBox().isSelected());
		context.setRegularExpression(FindReplaceFrame.getInstance().getRegexBox().isSelected());
		context.setWholeWord(FindReplaceFrame.getInstance().getWholeBox().isSelected());
		context.setSearchForward(forward);
		
		if ("FindNext".equals(command) || "FindPrev".equals(command)) {
			// FIND NEXT or FIND PREV
			boolean found = SearchEngine.find(rsta, context).wasFound();
			
			if (!found) {
				// nenajdene -> hlada sa na opacnej strane dokumentu
				if (!forward) {
					rsta.setCaretPosition(rsta.getText().length());
				}
				else {
					rsta.setCaretPosition(0);
				}
				found = SearchEngine.find(rsta, context).wasFound();
			}
			
			if (!found) {
				FindReplaceFrame.getInstance().getMatchLabel().setText("Not Found");
			}
			else {
				FindReplaceFrame.getInstance().getMatchLabel().setText("Found");
			}
		}
		else if ("ReplaceAll".equals(command)) {
			// REPLACE ALL
			context.setReplaceWith(FindReplaceFrame.getInstance().getReplaceField().getText());
			SearchResult found = SearchEngine.replaceAll(rsta, context);
			if (found.getCount() == 0) {
				FindReplaceFrame.getInstance().getMatchLabel().setText("Not Found");
			}
			else {
				FindReplaceFrame.getInstance().getMatchLabel().setText("Replaced " + found.getCount() + " instance(s)");
			}
		}
		else if ("Replace".equals(command)) {
			// REPLACE - hladanie vpred
			context.setReplaceWith(FindReplaceFrame.getInstance().getReplaceField().getText());
			boolean found = SearchEngine.replace(rsta, context).wasFound();
			if (!found) {
				// nenajdene -> skusene hladanie na opacnu stranu (vzad)
				context.setSearchForward(false);
				found = SearchEngine.replace(rsta, context).wasFound();
				if (!found) {
					// nenajdene ani vpred ani vzad
					FindReplaceFrame.getInstance().getMatchLabel().setText("Not Found");
				}
				else {
					FindReplaceFrame.getInstance().getMatchLabel().setText("Replaced");
				}
			}
			else {
				FindReplaceFrame.getInstance().getMatchLabel().setText("Replaced");
			}
		}
	

	 }
}
