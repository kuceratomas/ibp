package ahll.editor.actions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ahll.editor.gui.Frame;


public class CloseTabAction implements ActionListener {
	
	Frame frame;
	
	public CloseTabAction(Frame frame) {
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (frame.getTabbedPane().getTabCount() < 1) {
    		return;
    	}
    	
    	int index;
    	if ((e == null) || e.getSource().equals(frame.getGUIMenuBar().getItemClose()) || e.getSource().equals(frame.getGUIMenuBar().getItemCloseAll())) {
    		// zavretie z jmenu
    		index = frame.getTabbedPane().getSelectedIndex(); 
    	}
    	else {
    		// zatvorenie pomocou krizika na karte
    		index = frame.getTabbedPane().indexOfTabComponent((JPanel)((JButton)(java.awt.Component)e.getSource()).getParent());
    	}

    	File file =  frame.getTabCompAt(index).getTextArea().getFile();
    	String txtareatxt = frame.getTabCompAt(index).getTextArea().getText();
    	
    	if (file == null) {
    		if (txtareatxt.isEmpty()) {
    			// dokument nema subor a je prazdny
    			frame.getTabbedPane().remove(index);
    		}
    		else {
    			// dokument nema subor ale obsahuje text -> ulozit?
	    		int n = JOptionPane.showConfirmDialog(
                        frame, "File has not beed saved. Save changes?",
                        "Save changes?",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.WARNING_MESSAGE
                        );
	    		
	    		if (n == JOptionPane.YES_OPTION) {
	    			frame.getGUIMenuBar().getItemSaveAs().getActionListeners()[0].actionPerformed(e);
	    			if (frame.getTabCompAt(index).getTextArea().getFile() != null)
	    				frame.getTabbedPane().remove(index);
	    		}
	    		else if (n == JOptionPane.NO_OPTION) {
	    			frame.getTabbedPane().remove(index);
	    		}
    		}
    		
    	}
    	else {
    		// dokument ma subor -> skontrolovat jeho obsah s obsahom dokumentu, v pripade nezhody zobraz dialog
    		if(file.exists()) { 

				try {
					Scanner scanner = new Scanner(file, "UTF-8" );
					if ((!scanner.useDelimiter("\\Z").hasNext() && txtareatxt.equals("")) || // fixnut!!!!!!!!!!!!!!!!!!!!!!!!!!
						(scanner.useDelimiter("\\Z").hasNext() && scanner.useDelimiter("\\Z").next().equals(txtareatxt)))
						frame.getTabbedPane().remove(index);
			    	else {
			    		int n = JOptionPane.showConfirmDialog(
			    				frame, "File "+file.getName()+" has been modified. Save changes?",
	                            "Save changes?",
	                            JOptionPane.YES_NO_CANCEL_OPTION,
	                            JOptionPane.WARNING_MESSAGE
	                            );
			    		
			    		if (n == JOptionPane.YES_OPTION) {
			    			frame.getGUIMenuBar().getItemSave().getActionListeners()[0].actionPerformed(e);
			    			frame.getTabbedPane().remove(index);
			    		}
			    		else if (n == JOptionPane.NO_OPTION) {
			    			frame.getTabbedPane().remove(index);
			    		}
			    	}
			    	scanner.close();
				} catch (FileNotFoundException e1) {e1.printStackTrace();}
    		}
    		else {
    			int n = JOptionPane.showConfirmDialog(
	    				frame, "File "+file.getName()+" does not longer exist on disk. Save content of this document into new file?",
                        "File doesn't exist.",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.WARNING_MESSAGE
                        );
    			
    			if (n == JOptionPane.YES_OPTION) {
    				frame.getGUIMenuBar().getItemSaveAs().getActionListeners()[0].actionPerformed(e);
	    			frame.getTabbedPane().remove(index);
	    		}
	    		else if (n == JOptionPane.NO_OPTION) {
	    			frame.getTabbedPane().remove(index);
	    		}
    		}
    	}
	}
}
