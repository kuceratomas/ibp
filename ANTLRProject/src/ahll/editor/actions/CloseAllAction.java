package ahll.editor.actions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ahll.editor.gui.Frame;


public class CloseAllAction implements ActionListener{
	
	Frame frame;
	
	public CloseAllAction(Frame frame) {
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		int count = frame.getTabbedPane().getTabCount();
		int actual_tab = count - 1;
		for (int i = 0; i < count; i++) {
			// zatvorenie vsetkych kariet od poslednej po prvu
			frame.getTabbedPane().setSelectedIndex(actual_tab--);
			frame.getGUIMenuBar().getItemClose().getActionListeners()[0].actionPerformed(e);
		}
	}

}
