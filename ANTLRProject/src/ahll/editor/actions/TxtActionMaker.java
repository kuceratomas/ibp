package ahll.editor.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ahll.editor.gui.*;

public class TxtActionMaker {
	
	private TxtActionMaker() {
		
	}
	
	public static ActionListener makeRedoActionListener(Frame frame) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (frame.getTabbedPane().getSelectedIndex() == -1) {
					return;
				}
				
				frame.getActualTabComp().getTextArea().redoLastAction();
			}
			
		};
	}
	
	public static ActionListener makeUndoActionListener(Frame frame) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (frame.getTabbedPane().getSelectedIndex() == -1) {
					return;
				}
				
				frame.getActualTabComp().getTextArea().undoLastAction();
			}
			
		};
	}
	
	public static ActionListener makeCopyActionListener(Frame frame) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (frame.getTabbedPane().getSelectedIndex() == -1) {
					return;
				}
				
				frame.getActualTabComp().getTextArea().undoLastAction();
			}
			
		};
	}
	
	public static ActionListener makePasteActionListener(Frame frame) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (frame.getTabbedPane().getSelectedIndex() == -1) {
					return;
				}
				
				frame.getActualTabComp().getTextArea().paste();
			}
			
		};
	}
	
	public static ActionListener makeCutActionListener(Frame frame) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (frame.getTabbedPane().getSelectedIndex() == -1) {
					return;
				}
				
				frame.getActualTabComp().getTextArea().cut();
			}
			
		};
	}
	
	public static ActionListener makeDeleteActionListener(Frame frame) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (frame.getTabbedPane().getSelectedIndex() == -1) {
					return;
				}
				
				frame.getActualTabComp().getTextArea().replaceSelection("");
			}
			
		};
	}
	
	public static ActionListener makeWhiteCharActionListener(Frame frame) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (frame.getTabbedPane().getSelectedIndex() == -1) {
					return;
				}
				
				if (frame.getActualTabComp().getTextArea().isWhitespaceVisible()) {
					frame.getActualTabComp().getTextArea().setWhitespaceVisible(false);
					frame.getActualTabComp().getTextArea().setEOLMarkersVisible(false);
				}
				else {
					frame.getActualTabComp().getTextArea().setWhitespaceVisible(true);
					frame.getActualTabComp().getTextArea().setEOLMarkersVisible(true);

				}
			}
			
		};
	}
	
}
