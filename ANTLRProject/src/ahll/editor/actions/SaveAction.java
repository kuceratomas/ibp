package ahll.editor.actions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import ahll.editor.gui.Frame;


public class SaveAction implements ActionListener{

	Frame frame;
	
	public SaveAction(Frame frame) {
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
    	
    	if (frame.getTabbedPane().getSelectedIndex() == -1) {
    		return;
    	}
    	
    	// dokument nema subor -> ulozit ako
    	if (frame.getActualTabComp().getTextArea().getFile() == null || !frame.getActualTabComp().getTextArea().getFile().exists()) {
    		frame.getGUIMenuBar().getItemSaveAs().getActionListeners()[0].actionPerformed(e);
    	}
    	else {
    		
            try {
            	// vytvorenie suboru a skopirovanie obsahu dokumentu
            	File file = frame.getActualTabComp().getTextArea().getFile();
            	BufferedWriter output = new BufferedWriter(new FileWriter(file));
                output.write(frame.getActualTabComp().getTextArea().getText());
                output.close();
            } catch ( IOException e1 ) {e1.printStackTrace();}
    	}
    	
    }
}
