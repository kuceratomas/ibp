package ahll.editor.actions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ahll.editor.gui.Frame;
import ahll.editor.gui.TabComponent;


public class SaveAsAction implements ActionListener {
	
	Frame frame;
	
	public SaveAsAction(Frame frame) {
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
    	
    	if (frame.getTabbedPane().getSelectedIndex() == -1)
    		return;
    	
    	JFileChooser fc = new JFileChooser();
    	fc.setDialogTitle("Create AHLL");
    	int returnVal = fc.showSaveDialog(frame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	
        	((JLabel)((JPanel)frame.getTabbedPane().getTabComponentAt(frame.getTabbedPane().getSelectedIndex())).getComponent(0)).setText(fc.getSelectedFile().getName());
        	frame.getActualTabComp().getTextArea().setFile(fc.getSelectedFile());
        	
            try {
            	// skopirovanie obsahu dokumentu do suboru
                BufferedWriter output = new BufferedWriter(new FileWriter(fc.getSelectedFile()));
                output.write(((TabComponent)frame.getTabbedPane().getComponentAt(frame.getTabbedPane().getSelectedIndex())).getTextArea().getText());
                output.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
