package ahll.editor.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import ahll.editor.gui.Frame;

public class SimulateAction implements ActionListener{

Frame frame;
	
	public SimulateAction(Frame frame) {
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		

		Thread simulationThread = new Thread() {

			public void run() {
				try {
					
					String path = Frame.class.getProtectionDomain().getCodeSource().getLocation().getPath();
					String decodedPath = "";
					try {
						decodedPath = URLDecoder.decode(path, "UTF-8");
					} catch (UnsupportedEncodingException e1) {e1.printStackTrace();}
					
					File file2 = new File(decodedPath);
					decodedPath = file2.getParent();
					file2 = new File(decodedPath);
					
					ProcessBuilder pb = new ProcessBuilder("java", "-jar", decodedPath+"/Resources/lib/T-Mass/TMass-M.jar");
					pb.directory(file2);
					Process p = pb.start();
					
					BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
					
					if (frame.getTabbedPane().getSelectedIndex() == -1)
			    		return;
					
					String line = reader.readLine();
					frame.getActualTabComp().getJTextArea().setText("");
					while (line != null) {
						frame.getActualTabComp().getJTextArea().setText(line + "\n");
                        line = reader.readLine();
                    }
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		};
		simulationThread.start();
		
	}
}
