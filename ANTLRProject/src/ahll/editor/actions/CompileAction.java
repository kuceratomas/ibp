package ahll.editor.actions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Paths;

import ahll.editor.CompilationLog;
import ahll.editor.gui.Frame;


public class CompileAction implements ActionListener{

	Frame frame;
	
	public CompileAction(Frame frame) {
		this.frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (frame.getTabbedPane().getSelectedIndex() == -1) {
			return;
		}
		
		File file = frame.getActualTabComp().getTextArea().getFile();
		
		if (file == null) {
			return;
		}
		
		
		String fileName = file.getName();
		String newFileName;
		// ak ma priponu .ahll nahradi ju .alll, inak sa len pripoji .alll k nazvu
		if (0 == fileName.length() - fileName.replace(".ahll", "").length()) {
			newFileName = fileName + ".alll";
		}
		else {
			newFileName= fileName.replace(".ahll", ".alll");
		}
		
		String path = Frame.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String decodedPath = "";
		try {
			decodedPath = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e1) {e1.printStackTrace();}
		
		File file2 = new File(decodedPath);
		decodedPath = file2.getParent();
		file2 = new File(decodedPath);
		// nastavenie cesty k suboru na PWD
		File newFile = new File(Paths.get(".").toAbsolutePath().normalize().toString() + "\\" + newFileName);
		ProcessBuilder pb = new ProcessBuilder("java", "-ea", "-jar", decodedPath+"/Resources/lib/Compiler/ahll_v2.jar", file.getAbsolutePath().toString());

		pb.directory(file2);
		compile(pb, newFile);
		
	}
	
	private void compile(ProcessBuilder pb, File file)  {
		
		// spustenie prekladu v novom vlakne
		Thread compileThread = new Thread() {
			public void run() {

				try {
					Process p = pb.start();
	
					CompilationLog lsr = new CompilationLog(p.getInputStream(), p.getErrorStream(),
							frame.getActualTabComp().getJTextArea(), file, "None\n");

					lsr.intoFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		};
		
		compileThread.start();
	}
	
}
