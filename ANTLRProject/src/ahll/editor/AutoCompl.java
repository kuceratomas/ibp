package ahll.editor;

import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.autocomplete.CompletionProvider;

public class AutoCompl extends AutoCompletion{

	public AutoCompl(CompletionProvider provider) {
		super(provider);
	}
	
	@Override
	protected void insertCompletion(Completion c,
			boolean typedParamListStartChar) {
		
		super.insertCompletion(c, typedParamListStartChar);
		if (c.getClass().getName().equals("ahll.editor.ShorthandCompl")) {
			super.getTextComponent().setCaretPosition(super.getTextComponent().getCaretPosition() -
					((ShorthandCompl)c).getCaretPos());
		}
		
	}

}
