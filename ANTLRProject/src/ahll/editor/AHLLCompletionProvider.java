package ahll.editor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.fife.ui.autocomplete.*;


public class AHLLCompletionProvider extends LanguageAwareCompletionProvider {
	
	private DefaultCompletionProvider cp;
	List<Completion> dynamicCompletions = new ArrayList<Completion>();
	Set<String> dynamicCompletionNames = new HashSet<String>();
	
	public AHLLCompletionProvider() {
		setDefaultCompletionProvider(createCodeCompletionProvider());
		setCommentCompletionProvider(createCommentCompletionProvider());
	}
	
	protected CompletionProvider createCodeCompletionProvider() {
		cp = new DefaultCompletionProvider();
		
		cp.addCompletion(new BasicCompletion(cp, "if"));
		cp.addCompletion(new BasicCompletion(cp, "else"));
		cp.addCompletion(new BasicCompletion(cp, "while"));
		cp.addCompletion(new BasicCompletion(cp, "for"));
		cp.addCompletion(new BasicCompletion(cp, "foreach"));
		cp.addCompletion(new BasicCompletion(cp, "send"));
		cp.addCompletion(new BasicCompletion(cp, "receive"));
		cp.addCompletion(new BasicCompletion(cp, "call"));
		cp.addCompletion(new BasicCompletion(cp, "const"));
		cp.addCompletion(new BasicCompletion(cp, "return"));
		cp.addCompletion(new BasicCompletion(cp, "FI"));
		cp.addCompletion(new BasicCompletion(cp, "OI"));
		cp.addCompletion(new BasicCompletion(cp, "var"));
		cp.addCompletion(new BasicCompletion(cp, "in"));
		cp.addCompletion(new BasicCompletion(cp, "do"));
		cp.addCompletion(new BasicCompletion(cp, "null"));
		
		addShorthandCompletions(cp);
		return cp;

	}
	
	protected void addShorthandCompletions(DefaultCompletionProvider codeCP) {
		codeCP.addCompletion(new ShorthandCompl(codeCP, "main",
								"main() {\n\t\n}", "An entry point", 2));
		codeCP.addCompletion(new ShorthandCompl(codeCP, "plan",
				"plan (){\n\t\n}", "Plan definition", 7));
		codeCP.addCompletion(new ShorthandCompl(codeCP, "service",
				"service (){\n\t\n\treturn;\n}", "Service definition", 16));

	}
	
	protected CompletionProvider createCommentCompletionProvider() {
		DefaultCompletionProvider cp = new DefaultCompletionProvider();
		cp.addCompletion(new BasicCompletion(cp, "TODO:", "A to-do reminder"));
		cp.addCompletion(new BasicCompletion(cp, "FIXME:", "A bug that needs to be fixed"));
		return cp;
	}
	
	public void addDynamicCompletion(String name) {
		// ak sa tam dany identifikator nenachadza -> pridat ho
		if (dynamicCompletionNames.add(name)) {
			Completion compl = new BasicCompletion(cp, name);
			dynamicCompletions.add(compl);
			cp.addCompletion(compl);
		}
	}
	
	public void removeDynamicCompletions() {
		for (Completion c : dynamicCompletions) {
			cp.removeCompletion(c);
		}
		dynamicCompletionNames.clear();
	}
	


}
