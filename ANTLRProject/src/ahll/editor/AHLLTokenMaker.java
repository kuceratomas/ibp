package ahll.editor;
import java.util.*;

import javax.swing.text.Segment;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.TokenSource;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenImpl;
import org.fife.ui.rsyntaxtextarea.TokenMakerBase;

import ahll.editor.AParser.ProgramContext;
import ahll.editor.AParser.StartContext;


public class AHLLTokenMaker extends TokenMakerBase{
	
	@Override
	public boolean getCurlyBracesDenoteCodeBlocks(int languageIndex) {
		return true;
	}
	
	@Override
	public boolean getShouldIndentNextLineAfter(Token t) {
		if (t!=null && t.length()==1) {
			char ch = t.charAt(0);
			return ch=='{' || ch=='(';
		}
		return false;
	}


	private Token toList(Segment text, int startOffset, List<org.antlr.v4.runtime.Token> antlrTokens) {
		
		if (antlrTokens.isEmpty())
			return null;
		else {
			org.antlr.v4.runtime.Token at = antlrTokens.get(0);
			 TokenImpl t = new TokenImpl(text, text.offset + at.getStartIndex(), text.offset + at.getStartIndex() + at.getText().length() - 1, startOffset + at.getStartIndex(), at.getType(), 0);
			 t.setNextToken(toList(text, startOffset, antlrTokens.subList(1, antlrTokens.size())));
			 
			 return t;
		}
	}
	
	
	@Override
	public Token getTokenList(Segment text, int initialTokenType, int startOffset) {
		
		if (text == null)
			return null;
		
		Lexer lexer = new ALexer(CharStreams.fromString(text.toString()));
		LinkedList<org.antlr.v4.runtime.Token> tokens = new LinkedList<org.antlr.v4.runtime.Token>();
		CommonTokenStream tkns = new CommonTokenStream((TokenSource) lexer);
		AParser parser = new AParser(tkns);
		//parser.addParseListener(new VarListener());
        
        
		while (!lexer._hitEOF) {
			tokens.add(lexer.nextToken());


		}
		
		
		if (tokens.size() > 0) {
			StartContext sc = parser.start();

			ParseTreeWalker.DEFAULT.walk(new VarListener(), sc);

		}

		return (Token)toList(text, startOffset, tokens);
	}
}
