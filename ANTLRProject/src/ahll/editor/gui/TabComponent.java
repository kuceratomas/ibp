package ahll.editor.gui;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.fife.ui.rtextarea.RTextScrollPane;

import ahll.editor.AHLLCompletionProvider;
import ahll.editor.AutoCompl;
import ahll.editor.PlanAndServiceListProvider;


public class TabComponent extends JSplitPane{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	FileTextArea textArea;
	JSplitPane splitPane_1;
	DefaultListModel<String> listModel;
	JList<String> list;
	RTextScrollPane myTextScrollPane;
	JTextArea textArea_1;
	JLabel label;
	AHLLCompletionProvider provider;
	PlanAndServiceListProvider planAndIDProvider;
	
	public TabComponent(JLabel label) {
		
		this.label = label;
		
		textArea = new FileTextArea();
		myTextScrollPane = new RTextScrollPane(textArea);
		
		splitPane_1 = new JSplitPane();
		
		listModel = new DefaultListModel<String>();
		list = new JList<String>(listModel);
		
		textArea_1 = new JTextArea();
		
		planAndIDProvider = new PlanAndServiceListProvider();
		
		setProperties();
		setPlanListingAndDynamicIDs();
		
	}
	
	public FileTextArea getTextArea() {
		return textArea;
	}
	
	public JTextArea getJTextArea() {
		return textArea_1;
	}
	
	public JLabel getJLabel() {
		return label;
	}
	
	private void setProperties() {

		textArea.addCaretListener(new CaretListener(){
			@Override
			public void caretUpdate(CaretEvent e) {
				
				int row = textArea.getCaretLineNumber() + 1;
				int col = textArea.getCaretOffsetFromLineStart() + 1;
				label.setText(row + " : " + col);
			}
			
		});
		
		provider = new AHLLCompletionProvider();
		AutoCompl ac = new AutoCompl(provider);
		ac.install(textArea);
		
		textArea_1.setEditable(false);
		JScrollPane scrollPane_2 = new JScrollPane(textArea_1);
		scrollPane_2.setPreferredSize(new Dimension(scrollPane_2.getPreferredSize().width, 150));
		
		
		splitPane_1.setBottomComponent(scrollPane_2);
		splitPane_1.setTopComponent(myTextScrollPane);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_1.setResizeWeight(0.99);

		
		list.setVisibleRowCount(-1);
		JScrollPane scrollPane_1 = new JScrollPane(list);
		scrollPane_1.setPreferredSize(new Dimension(200, scrollPane_1.getPreferredSize().height));
		
		this.setRightComponent(splitPane_1);
		this.setLeftComponent(scrollPane_1);

		
	}
	
	private void setPlanListingAndDynamicIDs() {
		
		textArea.getDocument().addDocumentListener( new DocumentListener() {
		
			public void changedUpdate(DocumentEvent e) {
				planAndIDProvider.updatePlanAndCompletionList(listModel, provider, textArea);
			}
			public void removeUpdate(DocumentEvent e) {
				
			}
			public void insertUpdate(DocumentEvent e) {
				
			}
			
		});
		 
		
		list.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				@SuppressWarnings("unchecked")
				JList<String> list = (JList<String>)evt.getSource();
				String selected = (String) list.getSelectedValue();
				
				int index = planAndIDProvider.toPlan(selected);
	
				textArea.setCaretPosition(index);
				textArea.requestFocus();
		
			}
		});
	}
		

}
