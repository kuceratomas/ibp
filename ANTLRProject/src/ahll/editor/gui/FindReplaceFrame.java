package ahll.editor.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import ahll.editor.actions.FindAction;

public class FindReplaceFrame extends JInternalFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static FindReplaceFrame instance = new FindReplaceFrame();
	private Frame frame;
	private JTextField replaceField;
	private JTextField searchField;
	private JCheckBox matchCaseCB;
	private JCheckBox regexCB;
	private JCheckBox wholeCB;
	private JButton nextButton;
	private JButton prevButton;
	private JButton replaceAllButton;
	private JButton replaceButton;
	private JLabel matchFound;
	
	private FindReplaceFrame() {
		
		super("Find/Replace", false, true, false, false);

		nextButton = new JButton("Next");
		prevButton = new JButton("Previous");
		replaceButton = new JButton("Replace");
		searchField = new JTextField();
		replaceField = new JTextField();
		matchCaseCB = new JCheckBox("Case Sensitive");
		regexCB = new JCheckBox("Regular Ex");
		wholeCB = new JCheckBox("Whole word");
		replaceAllButton = new JButton("Replace All");
		matchFound = new JLabel(" ");
		
		
		
		setLayout(new GridBagLayout());
		setSize(280, 320);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 2;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(15, 25, 5, 0);
        gbc.weightx = 1;
		add(new JLabel("Find"), gbc);
		
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 2;
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(0, 5, 0, 5);
        gbc.weightx = 1;
		add(searchField, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 2;
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.insets = new Insets(15, 25, 5, 0);
        gbc.weightx = 1;
		add(new JLabel("Replace with"), gbc);
		
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.insets = new Insets(0, 5, 0, 5);
        gbc.weightx = 1;
		add(replaceField, gbc);

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.insets = new Insets(10, 0, 5, 0);
        gbc.weightx = 1;
		add(matchCaseCB, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.insets = new Insets(10, 0, 5, 0);
        gbc.weightx = 1;
		add(regexCB, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.insets = new Insets(5, 0, 10, 0);
        gbc.weightx = 1;
		add(wholeCB, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.insets = new Insets(0, 5, 10, 5);
        gbc.weightx = 1;
		add(nextButton, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.insets = new Insets(0, 5, 10, 5);
        gbc.weightx = 1;
		add(prevButton, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth =1;
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.insets = new Insets(0, 5, 0, 5);
        gbc.weightx = 1;
		add(replaceButton, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
        gbc.gridx = 1;
        gbc.gridy = 7;
        gbc.insets = new Insets(0, 5, 0, 5);
        gbc.weightx = 1;
		add(replaceAllButton, gbc);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.insets = new Insets(10, 5, 0, 5);
        gbc.weightx = 1;
		add(matchFound, gbc);
		
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addInternalFrameListener(new InternalFrameAdapter(){
            public void internalFrameClosing(InternalFrameEvent e) {
            	instance.setVisible(false);
            	instance.moveToBack();;
            }
        });
		
	}
	
	public static FindReplaceFrame getInstance(){
		return instance;
	}
	
	public JTextField getSearchField() {
		return searchField;
	}
	
	public JTextField getReplaceField() {
		return replaceField;
	}
	
	public JCheckBox getMatchBox() {
		return matchCaseCB;
	}
	
	public JCheckBox getRegexBox() {
		return regexCB;
	}
	
	public JCheckBox getWholeBox() {
		return wholeCB;
	}
	
	public JLabel getMatchLabel() {
		return matchFound;
	}


	public void setUp(Frame frame) {
		

		this.frame = frame;
		
	
		nextButton.setActionCommand("FindNext");
		replaceAllButton.setActionCommand("ReplaceAll");
		replaceButton.setActionCommand("Replace");;
		prevButton.setActionCommand("FindPrev");
		
		searchField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextButton.doClick(0);
			}
		});
		
		nextButton.addActionListener(new FindAction(frame));
		replaceAllButton.addActionListener(new FindAction(frame));
		replaceButton.addActionListener(new FindAction(frame));
	    prevButton.addActionListener(new FindAction(frame));
    
	 
		
	}
	
	public void showComp() {
		setLocation(frame.getDesktop().getSize().width/2, frame.getDesktop().getSize().height/2 - 150);
		setVisible(true);
		moveToFront();
		searchField.requestFocus();
	}
}
