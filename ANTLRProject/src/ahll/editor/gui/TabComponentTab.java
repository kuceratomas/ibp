package ahll.editor.gui;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import ahll.editor.actions.CloseTabAction;


public class TabComponentTab extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lblTitle;
    private JButton btnClose;
    private Frame frame;
		
	public TabComponentTab(String tabName, Frame frame) {
		
		this.frame = frame;

        lblTitle = new JLabel(tabName);
        btnClose = new JButton();
        
        setUp();
	}
	
	private void setUp() {
		setLayout(new GridBagLayout());
		setOpaque(false);
		
		btnClose.setContentAreaFilled(false);
        btnClose.setMargin(new Insets(0, 2, 0, 2));
        btnClose.setBorder(new EmptyBorder(0,0,0,0));
        btnClose.setFocusPainted(false);
        btnClose.setCursor(new Cursor(Cursor.HAND_CURSOR));
        btnClose.addActionListener(new CloseTabAction(frame));
        btnClose.setToolTipText("Close");
        
        Border border = lblTitle.getBorder();
        Border margin = new EmptyBorder(0,0,0,15);
        lblTitle.setBorder(new CompoundBorder(border, margin));
        lblTitle.setFont(new Font("verdana", Font.BOLD, 12));
        lblTitle.setForeground(new Color(0,204,204));

        try  {
            Image img = ImageIO.read(getClass().getResource("/images/remove.png"));
            btnClose.setIcon(new ImageIcon(img));
        } catch (IOException ex) {}

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        add(lblTitle, gbc);

        gbc.gridx++;
        gbc.weightx = 0;
        add(btnClose, gbc);
	}

	

}
