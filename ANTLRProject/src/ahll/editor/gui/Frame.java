package ahll.editor.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JTabbedPane;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.UIManager;

import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.folding.CurlyFoldParser;
import org.fife.ui.rsyntaxtextarea.folding.FoldParserManager;

import com.jtattoo.plaf.texture.TextureLookAndFeel;



public class Frame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JDesktopPane desktop;
	private MenuBar menuBar;
	private JTabbedPane tabbedPane;
	private Frame frame;
	private ToolBar toolBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				try {
					TextureLookAndFeel.setTheme("Rock", "", "");
					UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
		        }
		        catch (Exception ex) {
		            ex.printStackTrace();
		        }

				AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory)TokenMakerFactory.getDefaultInstance();
				atmf.putMapping("text/ahll", "ahll.editor.AHLLTokenMaker");
				FoldParserManager.get().addFoldParserMapping("text/ahll", new CurlyFoldParser());
				Frame frame = new Frame(args);
				frame.setVisible(true);
			}
		});
	}
	
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}
	
	public MenuBar getGUIMenuBar() {
		return menuBar;
	}
	
	public ToolBar getGUIToolBar() {
		return toolBar;
	}
	
	public JDesktopPane getDesktop() {
		return desktop;
	}
	
	public TabComponent getActualTabComp() {
		return ((TabComponent)tabbedPane.getComponentAt(frame.getTabbedPane().getSelectedIndex()));
	}
	
	public TabComponent getTabCompAt(int index) {
		return ((TabComponent)tabbedPane.getComponentAt(index));
	}

	/**
	 * Create the frame.
	 */
	public Frame(String[] args) {
		
		frame = this;
		contentPane = new JPanel(new BorderLayout());
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setPreferredSize(new Dimension(600,600));
		

		desktop = new JDesktopPane();
		//desktop.setLayout(new BorderLayout());
		desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		toolBar = new ToolBar(this);
		contentPane.add(toolBar, BorderLayout.NORTH);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (tabbedPane.getSelectedIndex() >= 0) {
					TabComponent tabComponent = (TabComponent)tabbedPane.getComponentAt(tabbedPane.getSelectedIndex());
					int row = tabComponent.getTextArea().getCaretLineNumber() + 1;
					int col = tabComponent.getTextArea().getCaretOffsetFromLineStart() + 1;
					tabComponent.getJLabel().setText(row + " : " + col);
					tabComponent.getTextArea().requestFocus();
				}
				else {
					getGUIToolBar().getLabel().setText("");
				}
			}
		});
		tabbedPane.setFocusable(false);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		WindowListener exitListener = new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
    			frame.getGUIMenuBar().getItemCloseAll().getActionListeners()[0].actionPerformed(null);
				if (tabbedPane.getTabCount() < 1) {
					System.exit(0);
				}
			}
		};
			 	
		menuBar = new MenuBar(this);
		
		FindReplaceFrame.getInstance().setUp(this);
		
		setTitle("AHLL Editor");
		setMinimumSize(new Dimension(600, 480));
		setSize(1024, 768);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		//setExtendedState(JFrame.MAXIMIZED_BOTH);
		addWindowListener(exitListener);
		setJMenuBar(menuBar);
		
		desktop.add(FindReplaceFrame.getInstance());
		desktop.add(contentPane);
		setContentPane(desktop);
		
		desktop.addComponentListener(new ComponentListener() {
			public void componentHidden(ComponentEvent arg0) {}
			
			public void componentMoved(ComponentEvent arg0) {}

			public void componentResized(ComponentEvent arg0) {
				contentPane.setSize(contentPane.getParent().getSize());
				FindReplaceFrame.getInstance().setLocation(frame.getDesktop().getSize().width/2, frame.getDesktop().getSize().height/2 - 150);
			}

			public void componentShown(ComponentEvent arg0) {}
			
		});
		
		// otvorenie so subormi specifikovanymi spustacimi parametrami
		for (String s: args) {
			initialFile(s);
        }

	}
	
	private void initialFile(String fileName) {
		// implicitna cesta vo filechooseri je PWD
		  
		File file = new File(fileName);
		
  		if(file.exists() && !file.isDirectory()) { 
  			
	  		TabComponent panel = new TabComponent(getGUIToolBar().getLabel());
	  		panel.getTextArea().setFile(file);
	  		getTabbedPane().add(file.getName(), panel);
	  		
	  		JPanel pnlTab = new TabComponentTab(file.getName(), this);
	
	  		getTabbedPane().setSelectedIndex( frame.getTabbedPane().getTabCount()-1);
	  		getTabbedPane().setTabComponentAt( frame.getTabbedPane().getTabCount()-1, pnlTab);
	
	  		try {
	  			// citanie obsahu suboru do dokumentu
	  			Scanner scanner = new Scanner(file, "UTF-8" );
	  			if (scanner.useDelimiter("\\Z").hasNext()) {
	  				panel.getTextArea().setText(scanner.useDelimiter("\\Z").next());
	  			}
	  			panel.getTextArea().requestFocus();
	  			scanner.close();
	  		} catch (FileNotFoundException e1) {e1.printStackTrace();}
  		}
	}


}
