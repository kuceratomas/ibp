package ahll.editor.gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import ahll.editor.actions.*;


public class MenuBar extends JMenuBar{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Frame frame;
	
	private JMenu menuFile;
	private JMenu menuEdit;
	private JMenu menuCompile;
	private JMenu menuSimulation;
	private JMenu menuHelp;
	
	private JMenuItem itemNew;
	private JMenuItem itemOpen;
	private JMenuItem itemClose;
	private JMenuItem itemCloseAll;
	private JMenuItem itemSave;
	private JMenuItem itemSaveAs;
	private JMenuItem itemSaveAll;
	
	private JMenuItem itemUndo;
	private JMenuItem itemRedo;
	private JMenuItem itemCopy;
	private JMenuItem itemPaste;
	private JMenuItem itemCut;
	private JMenuItem itemDelete;
	private JMenuItem itemFind;
	
	private JMenuItem itemCompile;
	private JMenuItem itemOptimizeAndCompile;
	
	private JMenuItem itemTmass;
	
	private JMenuItem itemAbout;

	public MenuBar(Frame frame) {
		
		this.frame = frame;
	
		menuFile = new JMenu("File");
		menuEdit = new JMenu("Edit");
		menuCompile = new JMenu("Compile");
		menuSimulation = new JMenu("Simulation");
		menuHelp = new JMenu("Help");
		
		itemNew = new JMenuItem("New File");
		itemOpen = new JMenuItem("Open File");
		itemSave = new JMenuItem("Save");
		itemSaveAs = new JMenuItem("Save As");
		itemSaveAll = new JMenuItem("Save All");
		itemClose = new JMenuItem("Close");
		itemCloseAll = new JMenuItem("Close All");
		
		itemUndo = new JMenuItem("Undo");
		itemRedo = new JMenuItem("Redo");
		itemCopy = new JMenuItem("Copy");
		itemPaste = new JMenuItem("Paste");
		itemCut = new JMenuItem("Cut");
		itemDelete = new JMenuItem("Delete");
		itemFind = new JMenuItem("Find/Replace");
		
		itemCompile = new JMenuItem("Compile");
		itemOptimizeAndCompile = new JMenuItem("Optimize and Compile");
		
		itemTmass = new JMenuItem("Run T-Mass");
		
		itemAbout = new JMenuItem("About");
		
		setUp();
		
	}
	
	private void setUp() {
		
		/* FILE */
		
		itemNew.addActionListener(new NewFileAction(frame));
		itemNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
		menuFile.add(itemNew);
		
		itemOpen.addActionListener(new OpenFileAction(frame));
		itemOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		menuFile.add(itemOpen);
		
		menuFile.addSeparator();
		
		itemSave.addActionListener(new SaveAction(frame));
		itemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		menuFile.add(itemSave);
		
		itemSaveAs.addActionListener(new SaveAsAction(frame));
		menuFile.add(itemSaveAs);
		
		itemSaveAll.addActionListener(new SaveAllAction(frame));
		menuFile.add(itemSaveAll);
		
		menuFile.addSeparator();
		
		itemClose.addActionListener(new CloseTabAction(frame));
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
		menuFile.add(itemClose);
		
		itemCloseAll.addActionListener(new CloseAllAction(frame));
		//itemCloseAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
		menuFile.add(itemCloseAll);
		
		/* EDIT */
		
		itemUndo.addActionListener(TxtActionMaker.makeUndoActionListener(frame));
		itemUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
		menuEdit.add(itemUndo);
		
		itemRedo.addActionListener(TxtActionMaker.makeRedoActionListener(frame));
		itemRedo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, ActionEvent.CTRL_MASK));
		menuEdit.add(itemRedo);
		
		menuEdit.addSeparator();
		
		itemFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FindReplaceFrame.getInstance().showComp();
			}
		});
		itemFind.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK));
		menuEdit.add(itemFind);
		
		menuEdit.addSeparator();
		
		itemCopy.addActionListener(TxtActionMaker.makeCopyActionListener(frame));
		itemCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		menuEdit.add(itemCopy);
		
		itemPaste.addActionListener(TxtActionMaker.makePasteActionListener(frame));
		itemPaste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
		menuEdit.add(itemPaste);
		
		itemCut.addActionListener(TxtActionMaker.makeCutActionListener(frame));
		itemCut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		menuEdit.add(itemCut);
		
		menuEdit.addSeparator();
		
		itemDelete.addActionListener(TxtActionMaker.makeDeleteActionListener(frame));
		itemDelete.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		menuEdit.add(itemDelete);
		
		
		
		/* COMPILE */
		
		itemCompile.addActionListener(new CompileAction(frame));
		itemCompile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		menuCompile.add(itemCompile);
		
		itemOptimizeAndCompile.addActionListener(new CompileAction(frame));
		//itemOptimizeAndCompile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		menuCompile.add(itemOptimizeAndCompile);
		
		
		/* SIMULATE */
		
		itemTmass.addActionListener(new SimulateAction(frame));
		//itemCompile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		menuSimulation.add(itemTmass);
		
		
		/* HELP */
		
		itemAbout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame,
					    "<html><center><b>eAHLL</b><br>Editor for Agent High Level Language<br><br>"
					    + "Author: Tomas Kucera<br>Brno University of Technology<br>Faculty of Information Technologies<br>"
					    + "Bachelor thesis<br><br>email: xkucer90@fit.vutbr.cz</center></html>",
					    "About",
					    JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		menuHelp.add(itemAbout);

		add(menuFile);
		add(menuEdit);
		add(menuCompile);
		add(menuSimulation);
		add(menuHelp);
	}
	
	public JMenuItem getItemSave() {
		return itemSave;
	}
	
	public JMenuItem getItemSaveAs() {
		return itemSaveAs;
	}
	
	public JMenuItem getItemClose() {
		return itemClose;
	}
	
	public JMenuItem getItemCloseAll() {
		return itemCloseAll;
	}
	
}
