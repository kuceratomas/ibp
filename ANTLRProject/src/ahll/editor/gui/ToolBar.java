package ahll.editor.gui;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JToolBar;

import ahll.editor.actions.*;


public class ToolBar extends JToolBar{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Frame frame;
	
	private JButton buttonAdd;
	private JButton buttonSave;
	private JButton buttonCompile;
	private JButton buttonUndo;
	private JButton buttonRedo;
	private JButton buttonFind;
	private JButton buttonSaveAll;
	private JButton buttonWhiteChar;
	private JButton buttonOptimizeCompile;
	private JLabel label;
	private JProgressBar bar;

	public ToolBar(Frame frame) {
		
		this.frame = frame;
	
		buttonAdd = new JButton();
		buttonSave = new JButton();
		buttonCompile = new JButton();
		buttonUndo = new JButton();
		buttonRedo = new JButton();
		buttonSaveAll = new JButton();
		buttonWhiteChar = new JButton();
		buttonOptimizeCompile = new JButton();
		buttonFind = new JButton();
		label = new JLabel();
		bar = new JProgressBar();
		
		setUp();
	}
	
	private void setUp() {
		
		buttonSetUp("/images/add.png", buttonAdd, new NewFileAction(frame), "New file");
		
		add(Box.createHorizontalStrut(10)); 
		
		buttonSetUp("/images/save.png", buttonSave, new SaveAction(frame), "Save");
		
		add(Box.createHorizontalStrut(10));
		
		buttonSetUp("/images/saveall.png", buttonSaveAll, new SaveAllAction(frame), "Save All");
		
		add(Box.createHorizontalStrut(10)); 
		
		buttonSetUp("/images/compile.png", buttonCompile, new CompileAction(frame), "Compile");
		
		add(Box.createHorizontalStrut(10));
		
		buttonSetUp("/images/optimize.png", buttonOptimizeCompile, new OptimizeCompileAction(frame), "Optimize and compile");
		
		add(Box.createHorizontalStrut(10));
		
		buttonSetUp("/images/search.png", buttonFind, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FindReplaceFrame.getInstance().showComp();
			}
		}
		, "Find/Replace");

		add(Box.createHorizontalStrut(10));
		
		buttonSetUp("/images/pilcrow.png", buttonWhiteChar, TxtActionMaker.makeWhiteCharActionListener(frame), "White characters");
		
		add(Box.createHorizontalStrut(20)); 
		
		buttonSetUp("/images/undo.png", buttonUndo, TxtActionMaker.makeUndoActionListener(frame), "Undo");

		add(Box.createHorizontalStrut(10));
		
		buttonSetUp("/images/redo.png", buttonRedo, TxtActionMaker.makeRedoActionListener(frame), "Redo");

		add(Box.createHorizontalStrut(20));

		label.setPreferredSize(new Dimension(100, 20));
		add(label);
		
		add(Box.createHorizontalStrut(20));
		
		bar.setMaximum(9);
		bar.setToolTipText("Compilation progress");
		bar.setValue(0);
		bar.setForeground(Color.GREEN);
		bar.setStringPainted(true);
		bar.setVisible(false);
		add(bar);

	}
	
	private void buttonSetUp(String path, JButton button, ActionListener listener, String tooltip) {
		try  {
            Image img1 = ImageIO.read(getClass().getResource(path));
            button.setIcon(new ImageIcon(img1));
        } catch (IOException ex) {}
		button.addActionListener(listener);
		button.setToolTipText(tooltip);
		add(button);
	}
	
	public JLabel getLabel() {
		return label;
	}
	
	public JProgressBar getProgressBar() {
		return this.bar;
	}
	
	
	
	
	
	
}
