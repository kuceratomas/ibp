package ahll.editor.gui;

import java.awt.Color;
import java.awt.Insets;
import java.io.File;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import ahll.editor.AHLLSyntaxScheme;

public class FileTextArea extends RSyntaxTextArea{
	private static final long serialVersionUID = 1L;
	File file = null;
	
	public FileTextArea() {
		
		super.setBackground(new Color(50, 50, 50));
		super.setForeground(Color.WHITE);
		super.setCurrentLineHighlightColor(new Color(70, 70, 70));
		super.setCaretColor(Color.WHITE);
		super.setTabSize(4);
		
		super.setSyntaxEditingStyle("text/ahll");

		//super.setCodeFoldingEnabled(true);
		super.setSyntaxScheme(new AHLLSyntaxScheme(true));
		
		//super.setAnimateBracketMatching(false);
		//super.setPaintMatchedBracketPair(false);
		//super.setMatchedBracketBGColor(Color.BLACK);
		//super.setMatchedBracketBorderColor(Color.YELLOW);
		super.setMargin(new Insets(0, 3, 0, 0));
		super.setPaintTabLines(true);
		
	}
	
	public File getFile() {
		return file;
	}
	
	public void setFile(File file) {
		this.file = file;
	}
}
