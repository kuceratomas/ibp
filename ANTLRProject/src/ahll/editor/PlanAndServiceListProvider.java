package ahll.editor;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;

import javax.swing.DefaultListModel;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.Lexer;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

public class PlanAndServiceListProvider {

	private Map<Integer, String> plans;
	
	public PlanAndServiceListProvider() {
		plans = new HashMap<Integer, String>();
	}
	
	
	public void updatePlanAndCompletionList(DefaultListModel<String> listModel, AHLLCompletionProvider provider, RSyntaxTextArea textArea) {
		
		String text = textArea.getText().toString();
		Lexer lexer = new ALexer(CharStreams.fromString(text.toString()));
		LinkedList<org.antlr.v4.runtime.Token> tokens = new LinkedList<org.antlr.v4.runtime.Token>();
			
		listModel.removeAllElements();
		plans.clear();
		
		provider.removeDynamicCompletions();
	
		while (!lexer._hitEOF) {
			tokens.add(lexer.nextToken());
			
			// pridanie tokenu do CompletionProvideru
			if ((tokens.get(tokens.size()-1).getType() == ALexer.Identifier)) {
				provider.addDynamicCompletion(tokens.get(tokens.size()-1).getText());
			}
			
			// pridanie tokenu do CompletionProvideru
			if ((tokens.get(tokens.size()-1).getText().equals("main"))) {
				plans.put(tokens.get(tokens.size()-1).getStartIndex(), "[m] main");
				listModel.addElement("[m] main");
			}
	
			// hladanie tokenu PLAN, neobmedzeny pocet WS a za nimi ID
			if ((tokens.size() > 2) && (tokens.get(tokens.size()-1).getType() == ALexer.Identifier)) {
				int i = 2;
				while (tokens.get(tokens.size()-i).getType() == ALexer.WS || tokens.get(tokens.size()-i).getType() == ALexer.NL) {
					i++;
					if (tokens.size() < i) {
						return;
					}
				}
				if (tokens.get(tokens.size()-i).getType() == ALexer.PLAN) {
					plans.put(tokens.get(tokens.size()-i).getStartIndex(), "[p] " + tokens.get(tokens.size()-1).getText());
					listModel.addElement("[p] " + tokens.get(tokens.size()-1).getText());
	
				}
				else if (tokens.get(tokens.size()-i).getType() == ALexer.SERVICE) {
					plans.put(tokens.get(tokens.size()-i).getStartIndex(), "[s] " + tokens.get(tokens.size()-1).getText());
					listModel.addElement("[s] " + tokens.get(tokens.size()-1).getText());
	
				}
			}
		}
	}
	
	// presunutie sa na plan/sluzbu v textArea
	public int toPlan(String selected) {
		
		int index = 0;
		
		for (Map.Entry<Integer, String> entry : plans.entrySet()) {
			if (Objects.equals(selected, entry.getValue())) {
				index = entry.getKey();
			}
		}
		
		return index;
	}
}
