package ahll.editor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.JTextArea;


public class CompilationLog {

        private BufferedReader reader;
        private BufferedReader reader2;
        JTextArea label;
        File file;
        String optimalizations;

        public CompilationLog(InputStream is, InputStream es, JTextArea label, File file, String optimalizations) {
            this.reader = new BufferedReader(new InputStreamReader(is));
            this.reader2 = new BufferedReader(new InputStreamReader(es));
            this.label = label;
            this.file = file;
            this.optimalizations = optimalizations;
        }

        public long read() {
        	label.setText("");
        	
            try {
            	
            	String line = reader2.readLine();

                if (line != null) {
                    return -1;
                }
            	
                line = "";
                line = reader.readLine();
                String code = "";
                while (line != null) {
                	code += line;
                    line = reader.readLine();
                }
                
                reader.close();
                reader2.close();
                
                return code.length();
                
            } catch (IOException e) {
                e.printStackTrace();
            }
            
			return -1;
        }
        
        public void intoFile() {
        	label.setText("");
        	
            try {
            	
            	String line = reader2.readLine();
                
                
                if (line == null) { // uspesna kompilacia
                	
                	line = reader.readLine();
                    String code = "";
                    while (line != null) {
                    	code += line + "\n";
                        line = reader.readLine();
                    }
                    
                    try {
                        BufferedWriter output = new BufferedWriter(new FileWriter(file));
                        output.write(code);
                        output.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    
                    label.setText("Applied optimalizations:\n" + optimalizations + "Compilation SUCCESSFUL\nOutput saved into: " + file.getAbsolutePath());

                }
                else { // chyba pri kompilacii
                	while (line != null) {
                        label.setText(line + "\n");
                        line = reader2.readLine();
                    }
                }
                
                reader.close();
                reader2.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
