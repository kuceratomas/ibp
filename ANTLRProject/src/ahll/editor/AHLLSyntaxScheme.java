package ahll.editor;
import java.awt.Color;

import org.fife.ui.rsyntaxtextarea.Style;
import org.fife.ui.rsyntaxtextarea.SyntaxScheme;


public class AHLLSyntaxScheme extends SyntaxScheme{
		   
	public AHLLSyntaxScheme(boolean useDefaults) {
		super(useDefaults);
	}

	@Override
	public Style getStyle(int index)  {
		
	    Style style = new Style();
	
	    switch (index) {
	        		
	    	case ALexer.COMMENT :
	    		style.foreground = Color.GRAY;
	    		break;
	    		
	    	case ALexer.IntegerLiteral :
	    	case ALexer.StringLiteral:
	    		style.foreground = Color.MAGENTA;
	    		break;
	    		  	
	    	case ALexer.CONSTANT :
	    		style.foreground = Color.YELLOW;
	        	break;
	        	

	    	case ALexer.PZ :
	    	case ALexer.PZZ:
	    	case ALexer.LZ:
	    	case ALexer.LZZ:
	    	case ALexer.PHZ:
	    	case ALexer.LHZ:
	    		style.foreground = Color.WHITE;
	        	break;
	        	
	    	case ALexer.Identifier:
	    		style.foreground = new Color(220,255,200);
	    		break;
	        	
	    	case ALexer.ADD:
	    	case ALexer.SUB:
	    	case ALexer.SEND:
	    	case ALexer.RECEIVE:
	    	case ALexer.ASSIGN:
	    	case ALexer.EQ:
	    	case ALexer.IN :
	    	case ALexer.NULL:
	    		style.foreground = Color.PINK;
	        	break;
	        	
	    	case ALexer.PLAN :
	    	case ALexer.SERVICE :
	    		style.foreground = Color.CYAN;
	        	break;
	        	
	        
	    	case ALexer.UNMATCHED :
	    		style.foreground = Color.RED;
	    		style.underline = true;
	        	break;
	        	
	    	default:
	    		style.foreground = Color.WHITE;
	        	break;

	    }
    
	    return style;
	}
	   
	   
	};

