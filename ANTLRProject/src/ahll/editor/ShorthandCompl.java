package ahll.editor;

import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.ShorthandCompletion;

public class ShorthandCompl extends ShorthandCompletion{
	
	private int caretPos;
	
	public ShorthandCompl(CompletionProvider provider, String inputText,
			String replacementText, String shortDesc, int caretPos) {
		super(provider, inputText, replacementText, shortDesc);
		this.caretPos = caretPos;
	}
	
	public int getCaretPos() {
		return caretPos;
	}


}
