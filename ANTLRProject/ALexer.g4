lexer grammar ALexer;

@header { package ahll.editor; }

MAIN : 'main' ;
PLAN : 'plan' ;
SERVICE : 'service';
THROWS : 'throws';
IF : 'if';
ELSE : 'else';
WHILE : 'while' ;
FOR : 'for';
FOREACH : 'foreach';
VAR : 'var';
CONSTANT: 'const';
NULL : 'null'|'NULL';
SEND : 'send';
RECEIVE : 'receive';
CALL : 'call';
RETURN : 'return';
FI : 'FI' ;
OI : 'OI';
IN : 'IN' ;

/* Operators ---------------------------------------------------------------- */

ASSIGN : '=' ;
OR : '||' ;
AND : '&&' ;
EQ : '==' ;
NEQ : '!=' ;
LS : '<' ;
LSE : '<=' ;
GT : '>' ;
GTE : '>=' ;
ADD : '+' ;
SUB : '-' ;
MUL : '*' ;
DIV : '/' ;
MOD : '%' ;
NEG : '!' ;
INC : '++' ;
DEC : '--' ;
CIARKA : ',' ;
LZ : '(' ;
PZ : ')' ;
LZZ : '{' ;
PZZ : '}' ;
PHZ : ']' ;
LHZ : '[' ;

/* Others ------------------------------------------------------------------- */
StringLiteral: '"' LetterOrDigit* '"' ;
IntegerLiteral : Digits ;
fragment Digits : Digit+ ;
fragment Digit : [0-9] ;
Identifier : Letter LetterOrDigit* ;
fragment Letter : [a-zA-Z] ;
fragment LetterOrDigit : [a-zA-Z0-9] ;
NL : '\r'? '\n' -> channel(1) ;
SC : ';' ;
WS : (' '|'\t'|'\r'|'\n')+ -> channel(1) ;
COMMENT :
( ('//' .*? NL)
| ('/*' .*? '*/')
) -> channel(1) ;

UNMATCHED          			: . -> channel(1);